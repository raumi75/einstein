<?php
$aktiveseite = 'anmelden.php';
include 'head.php';
$fehlermeldungInvalidNickname = '';
$fehlermeldungInvalidCode = '';
$classInvalidNickname = '';
$classInvalidCode = '';
$classInvalidPassword = '';
$nickname = trim(filter_input(INPUT_POST, 'nickname', FILTER_SANITIZE_STRING));
$code = trim(filter_input(INPUT_POST, 'code', FILTER_SANITIZE_STRING));
$password = trim(filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING));
$password2 = trim(filter_input(INPUT_POST, 'password2', FILTER_SANITIZE_STRING));

if (binAngemeldet()){
    unset($_SESSION['nickname']);
}

if (!empty($_POST)) {
    if (empty($code)) {
        $classInvalidCode = 'is-invalid';
    }
    if (empty($nickname)) {
        $classInvalidNickname = 'is-invalid';
        $fehlermeldungInvalidNickname = 'Der Nickname ist leer.';
    }
    if (empty($password)) {
        $classInvalidPassword = 'is-invalid';
    }
    if (existiertNickname($nickname)) {
        $fehlermeldungInvalidNickname = 'Deinen Nickname gibt es schon. Versuche einen anderen.';
        $classInvalidNickname = 'is-invalid';
    }
    if ($code !== '5245854') {
        $classInvalidCode = 'is-invalid';
    }
    if ($password !== $password2) {
        $classInvalidPassword = 'is-invalid';
    }
    if (empty($classInvalidNickname) and empty($classInvalidPassword) and empty($classInvalidCode)) {
        registerUser($nickname, $password);
        echo "<script>window.location.href = \"./anmelden.php\";</script>";
    }
}

echo '
<div class="container mt-lg-5 mt-xs-0">
    <div class="row">
        <div class="col-sm">
            <h1>Registrieren</h1>
        </div>
    </div>
    <form method="post">
        <div class="form-group">
            <label for="exampleFormControlText">Nickname</label>
            <input type="text"
                   class="form-control ' .  $classInvalidNickname . '"
                   id="exampleFormControlText"
                   name="nickname"
                   value="' . $nickname . '">
            <div class="invalid-feedback">
                '. $fehlermeldungInvalidNickname .'              
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword4">Password</label>
            <input type="password"
                   class="form-control ' . $classInvalidPassword . '"
                   id="inputPassword4"
                   name="password"
            >
        </div>
        <div class="form-group">
            <label for="inputPassword4">Password nocheinmal eingeben</label>
            <input type="password"
                   class="form-control ' . $classInvalidPassword . '"
                   id="inputPassword4"
                   name="password2"
            >
        </div>
        <div class="form-group">
            <label for="inputCode">Code</label>
            <input type="text"
                   class="form-control ' . $classInvalidCode . '"
                   id="inputCode"
                   name="code"
            >
            <div class="invalid-feedback">
                '. $fehlermeldungInvalidCode .'              
            </div>
        </div>
        <button type="submit" class="btn btn-info">Registrieren</button>
    </form>
</div>
';

include 'fuss.php';
?>