<?php
include 'head.php';

$fehlermeldungInvalidNickname = '';
$classInvalidNickname = '';
$classInvalidEmail = '';
$classInvalidPassword = '';
$nickname = trim(filter_input(INPUT_POST, 'nickname', FILTER_SANITIZE_STRING));
$emailaddress = trim(filter_input(INPUT_POST, 'emailaddress', FILTER_SANITIZE_EMAIL));
$password = trim(filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING));

if (binAngemeldet()){
    unset($_SESSION['nickname']);
}
if (istPasswortKorrekt($nickname, $password)) {
    echo '<div class="alert alert-success" role="alert">
  Password richtig. Du bist angemeldet.
</div>';
    $_SESSION['nickname'] = $nickname;
    echo "<script>window.location.href = \"./index.php\";</script>";
}
echo '
<div class="container mt-lg-5 mt-xs-0">
    <div class="row">
        <div class="col-sm">
            <h1>Anmelden</h1>
        </div>
    </div>
    <form action="anmelden.php" method="post">
        <div class="form-group">
            <label for="exampleFormControlText">Nickname</label>
            <input type="text"
                   class="form-control ' .  $classInvalidNickname . '"
                   id="exampleFormControlText"
                   name="nickname"
                   value="' . $nickname . '">
            <div class="invalid-feedback">
                '. $fehlermeldungInvalidNickname .'              
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword4">Password</label>
            <input type="password"
                   class="form-control ' . $classInvalidPassword . '"
                   id="inputPassword4"
                   name="password"
            >
        </div>
        <button type="submit" class="btn btn-info">Anmelden</button>
        ';
        if (!binAngemeldet()){
            echo'Neu hier? 
            <a href="registrieren.php">Registrieren</a>';
        }
        echo '
    </form>
</div>
';
include 'fuss.php';
echo '

</body>
</html>';