<?php
$story_id = trim(filter_input(INPUT_GET, 'story_id', FILTER_SANITIZE_NUMBER_INT));

$aktiveseite = 'Geschichte '.$story_id.' löschen';
include 'head.php';
$row = ladeStory($story_id);

if ( !darfUserBearbeiten($row['nickname']) ) {
    echo "Nur der Author kann Geschichten löschen.";
    die();
}
$pdo = new PDO('sqlite:database');
$stmt = $pdo->prepare('DELETE FROM stories WHERE id = ?;');
if (!$stmt->execute([$story_id])) {
    echo 'Fehler!';
    print_r($stmt->errorInfo());
}

if ($stmt->rowCount()) {
    echo ' 
<div class="container mt-lg-5 mt-xs-0">
    <div class="row">
        <div class="col-sm">
            <h1>Story gelöscht</h1>
        </div>
     </div>
 </div>';
} else {
    echo ' 
<div class="container mt-lg-5 mt-xs-0">
    <div class="row">
        <div class="col-sm">
            <h1>Fehler: Konnte nicht gelöscht werden</h1>
            <p>Angemeldet: '.$_SESSION['nickname'].' Userid '.userIdAngemeldet().'</p>
        </div>
     </div>
 </div>';
}

include 'fuss.php';
echo '</body></html>';