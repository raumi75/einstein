<?php
//echo phpinfo();
$aktiveseite = 'Geschichte schreiben';
include 'head.php';
$pdo = new PDO('sqlite:database');
$inhalt = trim(filter_input(INPUT_POST, 'inhalt', FILTER_SANITIZE_STRING));
$title = trim(filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING));
$category_id = trim(filter_input(INPUT_POST, 'category_id', FILTER_SANITIZE_NUMBER_INT));

if ($_POST) {
    if (strlen($title) < 1) {
        $classInvalidUeberschrift = 'is-invalid';
    }
    if (strlen($inhalt) < 1) {
        $classInvalidInhalt = 'is-invalid';
    }
} else {
    $classInvalidUeberschrift = '';
    $classInvalidInhalt = '';
}

if (empty($classInvalidUeberschrift) and empty($classInvalidInhalt) and ($_POST)) {
    // speichern
    $sql = "INSERT INTO stories (title, inhalt, category_id, user_id) VALUES (?, ?, ?, ?)";
    $stmt = $pdo->prepare($sql);
    if ($stmt->execute([$title, $inhalt, $category_id, nicknameZuUser_id($_SESSION['nickname'])])) {
        echo '<p>Erfolgreich gespeichert. <a href="index.php" class="btn btn-primary btn-lg">Zur Startseite</a></p>';
    } else {
        echo '<p class="alert alert-danger">konnte nicht speichern.</p>';
        print_r($stmt->errorInfo());
    }
} else {
    if (binAngemeldet()){

        echo '
    <div class="container mt-lg-5 mt-xs-0">
        <div class="row">
            <div class="col-sm">
                <h1>Geschichte schreiben</h1>
            </div>
        </div>
        <form action="schreiben.php" method="post">
            ';
        categorySelect();
        echo'
            <div class="form-group">
                <label for="formControlTextUeberschrift1">Überschrift</label>
                <input class="form-control ' . $classInvalidUeberschrift . ' form-control-sm "
                       id="formControlTextUeberschrift1" type="text" name="title"
                       value="' . $title . '">
                <div class="invalid-feedback">
                    Überschrift fehlt
                </div>
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Text</label>
                <textarea class="form-control ' . $classInvalidInhalt . '" id="exampleFormControlTextarea1"
                          rows="6"
                          name="inhalt">' . $inhalt . '</textarea>
                <div class="invalid-feedback">
                    Inhalt fehlt
                </div>
            </div>
            <button type="submit" class="btn btn-info">speichern</button>
        </form>
    </div>';
    }
    else{
        echo '<div class="alert alert-primary" role="alert">
  Nur angemeldete Benutzer dürfen Geschichten schreiben. 
  <a href="anmelden.php" class="btn btn-primary btn-lg">Jetzt anmelden</a>
  </div>';
    }

}

    include 'fuss.php';
echo '
</body>
</html>';