<?php
$aktiveseite = 'bearbeiten.';
    include 'head.php';
$story_id = trim(filter_input(INPUT_GET, 'story_id', FILTER_SANITIZE_NUMBER_INT));

$inhalt = trim(filter_input(INPUT_POST, 'inhalt', FILTER_SANITIZE_STRING));
$title = trim(filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING));
$category_id = trim(filter_input(INPUT_POST, 'category_id', FILTER_SANITIZE_NUMBER_INT));
if ($_POST) {
    if (strlen($title) < 1) {
        $classInvalidUeberschrift = 'is-invalid';
    }
    if (strlen($inhalt) < 1) {
        $classInvalidInhalt = 'is-invalid';
    }
} else {
    $classInvalidUeberschrift = '';
    $classInvalidInhalt = '';
}

$row = ladeStory($story_id);

if ( !darfUserBearbeiten($row['nickname']) ) {
    echo "Nur der Author kann Geschichten ändern.";
    die();
}
if (empty($classInvalidUeberschrift) and empty($classInvalidInhalt) and ($_POST)) {
    // speichern
    $pdo = new PDO('sqlite:database');
    $sql = "UPDATE stories SET title= ?, inhalt= ?, category_id= ? WHERE id=?";
    $stmt = $pdo->prepare($sql);
    if ($stmt->execute([$title, $inhalt, $category_id, $story_id])) {
        echo "<script>window.location.href = \"lesen.php?story_id=$story_id\";</script>";

        echo '<p>Erfolgreich bearbeitet. <a href="lesen.php?story_id='.$story_id.'" class="btn btn-primary btn-lg">Zur Startseite</a></p>';
    } else {
        echo '<p class="alert alert-danger">Konnte nicht bearbeitet werden.</p>';
        print_r($stmt->errorInfo());
    }
}else {
        echo '
    <div class="container mt-lg-5 mt-xs-0">
        <div class="row">
            <div class="col-sm">
                <h1>Geschichte bearbeiten</h1>
            </div>
        </div>
        <form action="bearbeiten.php?story_id=' . $story_id . '" method="post">
            ';
        categorySelect('Kategorie', $row['category_id'] );
        echo'
            <div class="form-group">
                <label for="formControlTextUeberschrift1">Überschrift</label>
                <input class="form-control ' . $classInvalidUeberschrift . ' form-control-sm "
                       id="formControlTextUeberschrift1" type="text" name="title"
                       value="' . $row['title'] . '">
                <div class="invalid-feedback">
                    Überschrift fehlt
                </div>
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Text</label>
                <textarea class="form-control ' . $classInvalidInhalt . '" id="exampleFormControlTextarea1"
                          rows="6"
                          name="inhalt">' . $row['inhalt'] . '</textarea>
                <div class="invalid-feedback">
                    Inhalt fehlt
                </div>
            </div>
           
            <button type="submit" class="btn btn-info">speichern</button>
           
        </form>
    </div>';
}