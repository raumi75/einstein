<?php
require __DIR__ . '/vendor/autoload.php';
use Michelf\Markdown;

$story_id = trim(filter_input(INPUT_GET, 'story_id', FILTER_SANITIZE_NUMBER_INT));

$aktiveseite = 'Geschichte '.$story_id.' lesen';
include 'head.php';
$row = ladeStory($story_id);
if ($row){
echo' 
<div class="container mt-lg-5 mt-xs-0">
    <div class="row">
        <div class="col-sm">
            <h1>' . $row['title'] . ' <small>(von ' . $row['nickname'] . ')</small></h1>
            <p class="lead">' . Markdown::defaultTransform( $row['inhalt']) . '</p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm">
         ';
if (darfUserBearbeiten($row['nickname'])) {
    echo ' <a href="loeschen.php?story_id=' . $row['id'] . '">Löschen</a>';
    echo ' <a href="bearbeiten.php?story_id=' . $row['id'] . '">Bearbeiten</a>';
}


echo'

        
        </div>
    </div>
</div> 
    
';
}
include 'fuss.php';
echo '</body></html>';