<?php
session_start();
include_once 'lib/authentication.php';

$seiten['index.php'] = 'Startseite';
$seiten['schreiben.php'] = 'Geschichte schreiben';

if (binAngemeldet()){
    $seiten['anmelden.php'] =  $_SESSION['nickname'] . ' abmelden';
}
else {
    $seiten['anmelden.php'] = 'Anmelden';
}
$aktiverDateiname = basename($_SERVER['SCRIPT_FILENAME']);
$aktiveseite = $seiten[$aktiverDateiname] ?? $aktiveseite;

?>
<!doctype html>
<html lang="de">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>Deinstein-Stories.<?php echo $aktiveseite; ?></title>
</head>
<body>
<div class="container-fluid">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="./index.php">Deinstein-Stories</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <?php
                foreach ($seiten as $dateiname => $seitenname) {
                    echo "<li class=\"nav-item ";
                    if ($dateiname == $aktiverDateiname) {
                        echo 'active';
                    }
                    echo "\">
                    <a class=\"nav-link\" href=\"$dateiname\">$seitenname</a>
                </li>";
                }

                ?>
            </ul>
        </div>
    </nav>
</div>
