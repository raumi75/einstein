<?php

function binAngemeldet(): bool {
    return array_key_exists('nickname', $_SESSION);
}

function istPasswortKorrekt($nickname, $password): bool {
    $korrektesPasswort = passwortMd5VonUser($nickname);
    return (hashPassword($password) === $korrektesPasswort);
}

function passwortMd5VonUser($nickname):string {
    $pdo = new PDO('sqlite:database');
    $stmt = $pdo->prepare('SELECT password FROM users where nickname=?;');
    $stmt->execute([$nickname]);
    return $stmt->fetchColumn();
}

function hashPassword($password)
{
    return md5($password . 'E1nst3in');
}

function registerUser($nickname, $password)
{
    $password = hashPassword($password);
    $pdo = new PDO('sqlite:database');

    $stmt = $pdo->prepare('insert into users (nickname, password) VALUES (?,?);');
    if ($stmt->execute([$nickname, $password])) {
        echo 'Benutzer erfolgreich gespeichert';
    } else {
        echo "Fehler beim Speichern";
        print_r($stmt->errorInfo());
    }
}

function existiertNickname($nickname)
{
    $pdo = new PDO('sqlite:database');
    $stmt = $pdo->prepare('SELECT nickname FROM users where nickname=?;');
    $stmt->execute([$nickname]);
    return (bool) $stmt->fetchColumn();
}

function nicknameZuUser_id($nickname)
{
    $pdo = new PDO('sqlite:database');
    $stmt = $pdo->prepare('SELECT id FROM users where nickname=?;');
    $stmt->execute([$nickname]);
    return $stmt->fetchColumn();
}

function userIdAngemeldet():int
{
    if (binAngemeldet()) {
        return nicknameZuUser_id($_SESSION['nickname']);
    } else {
        return -1;
    }
}

function categories() {
    $pdo = new PDO('sqlite:database');
    return $pdo->query('SELECT * FROM categories;', PDO::FETCH_ASSOC)->fetchAll();
}

function categorySelect($title='Kategorie', $selectedId=-1){
    $rows = categories();
    echo'<div class="form-group">
                <label for="exampleFormControlSelect1">'.$title.'</label>
                <select class="form-control" id="exampleFormControlSelect1" name="category_id">
        ';
foreach ($rows as $row) {
    echo '<option ';
    if ($selectedId === $row['id']) {
        echo "selected ";
    }
    echo'
    value="' . $row['id'] . '">' . $row ['name'] . '</option>';
}
echo '
                </select>
            </div>';
}

function categoryLinks($aktiveId=-1)
{

    $rows = categories();
    $rows[-1]['id'] = -1;
    $rows[-1]['name'] = 'Alle';
    asort($rows);
    echo '
<ul class="nav nav-tabs">
';
    foreach ($rows as $row) {
        echo '<li class="nav-item">
              <a class="nav-link ';
        if ($aktiveId === $row['id']) {
            echo "active ";
        }

        echo'" href="index.php?category_id=' . $row['id'] . '">' . $row ['name'] . '</a>
              </li> 
              ';
    }
    echo'</ul>';
}

function darfUserBearbeiten($author){
    return
    (binAngemeldet() and
        ($_SESSION['nickname'] === $author
            or $_SESSION['nickname'] === 'Jolue'));
}

function ladeStory($story_id) {
    $pdo = new PDO('sqlite:database');
    $stmt = $pdo->prepare('SELECT s.*, c.name as category_name , u.nickname
FROM stories s 
LEFT JOIN categories c on s.category_id = c.id
LEFT JOIN users u on s.user_id = u.id
WHERE s.id = ?');
    $stmt->execute([$story_id]);
    return $stmt->fetch(PDO::FETCH_ASSOC);
}