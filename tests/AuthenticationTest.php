<?php
use PHPUnit\Framework\TestCase;

include_once __DIR__.'/../lib/authentication.php';

class AuthenticationTest extends TestCase
{
    public function testIstPasswortKorrekt()
    {
        $this->assertTrue(istPasswortKorrekt('jolue', '1234asdf'));
    }

    public function testIstPasswortNichtKorrekt()
    {
        $this->assertFalse(istPasswortKorrekt('jonlue', 'f'));
    }

    public function testPasswortMd5VonUserGefunden() {
        $this->assertEquals(
            '383ba2afbfbcbe99281e13b76d7f8602',
            passwortMd5VonUser('jolue')
        );
    }

    public function testExistiertNicknameFalse()
    {
        $this->assertFalse(existiertNickname('gibtsdochgarnichtBielefeld'));
    }

    public function testExistiertNicknameTrue()
    {
        $this->assertTrue(existiertNickname('jolue'));
    }

    public function testNicknameZuUser_id()
    {
        $this->assertEquals(7, nicknameZuUser_id('jan'));
    }
}