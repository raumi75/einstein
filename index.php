<?php
$aktiveseite = 'Neue Geschichten';
$category_id = trim(filter_input(INPUT_GET, 'category_id', FILTER_SANITIZE_NUMBER_INT));

include 'head.php';
$pdo = new PDO('sqlite:database');
$sql = 'SELECT s.*, c.name as category_name , u.nickname
FROM stories s 
LEFT JOIN categories c on s.category_id = c.id
LEFT JOIN users u on s.user_id = u.id
';
if ($category_id>-1 and !empty($category_id)) {
    $sql .= " WHERE category_id = ? ";
    $filter_array = [$category_id];
} else {
    $filter_array = null;
    $category_id = -1;
}
$stmt = $pdo->prepare($sql);
$stmt->execute($filter_array);
$rows = $stmt->fetchAll( PDO::FETCH_ASSOC);
echo'

<div class="container mt-lg-5 mt-xs-0">
    <div class="row" style="margin-bottom: 1em;">
        <div class="col-sm">
            <h1>Startseite</h1>
           ';
if (!binAngemeldet()){
    echo'
<div class="jumbotron">
  <h1 class="display-4">Neu hier?</h1>
  <p class="lead">Hallo und willkommen bei Deinstein-Stories, wenn du hier Geschichten veröffentlichen möchtest kannst du auf den folgenden Button klicken und dich anmelden. Jedoch brauchst du dafür den Geheimcode. </p>
  <a href="registrieren.php" class="btn btn-primary btn-lg" href="#" role="button">Registrieren</a>
</div>'
    ;
}
echo'
            <button class="btn btn-light" type="button" id="alleAnzeigen"
                    aria-expanded="false" aria-controls="collapseExample1">
                Alle anzeigen
            </button>
            <button class="btn btn-light" type="button" id="alleVerstecken"
                    aria-expanded="false" aria-controls="collapseExample1">
                Alle verstecken
            </button>
        </div>
    </div>
    <h4>Kategorien</h4>
'; categoryLinks($category_id);
    echo'
    <p></p>
    <div class="row">
    ';
        foreach ($rows as $row) {
            $id = $row['id'];
            echo '
        <div class="col-md-4 col-sm-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <button class="btn btn-lg btn-light btn-block" type="button" data-toggle="collapse"
                            data-target="#collapseStory' . $id . '" aria-expanded="false" aria-controls="collapseStory' . $id . '">
                       ' . $row ['title'] . '
                    </button>
                    <div class="collapse collapseStory" id="collapseStory' . $id . '">
                        <p class="card-text">' . $row['inhalt'] . '</p>
                    </div>
                    <a class="badge badge-secondary" href="index.php?category_id='.$row['category_id'].'">' . $row['category_name'] . '</a>
                     <span class="float-right badge badge-info">' . $row['nickname'] . '</span>
                     <a href="lesen.php?story_id=' . $id . '">Lesen</a>
                </div>
            </div>
        </div>';

        }

        ?>
    </div>
</div>
<?php
include 'fuss.php';
?>
<script src="index.js"></script>
</body>
</html>